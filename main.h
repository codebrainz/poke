#pragma once

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN 1
#define VC_EXTRALEAN 1
#define UNICODE 1
#define _UNICODE 1
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#else
#error "Only windows is supported so far"
#endif

enum test
{
    TEST_ICMP_ECHO = 1,
    TEST_TCP_CONNECT = 2,
    TEST_TCP_HALF_OPEN = 4,
    TEST_TCP_INTERNET = 8, // tcp://8.8.8.8:53
};

enum beep_mode
{
    BEEP_MODE_NONE = 0,
    BEEP_MODE_PASS = 1,
    BEEP_MODE_FAIL = 2,
    BEEP_MODE_ALL = (BEEP_MODE_PASS | BEEP_MODE_FAIL),
};

struct options
{
    char *host;
    uint16_t port;
    enum test test;
    uint32_t count;
    uint32_t timeout;
    uint32_t delay;
    enum beep_mode beep;
};

void options_parse(struct options *opts, int argc, char **argv);

void console_init(void);
void console_finalize(void);
void console_reset(void);
void console_set_color(int color);
void console_printf(const char *fmt, ...);

void network_init(void);
void network_finalize(void);
bool network_connect(const char *host, uint16_t port, int timeout);

void utils_beep(int freq, int duration);
void utils_sleep(int ms);
