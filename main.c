#include "main.h"

#include <stdlib.h>

#define PASS_FREQ 750
#define FAIL_FREQ 250
#define BEEP_DURATION 250

int main(int argc, char **argv)
{
    struct options opts;

    console_init();
    network_init();

    options_parse(&opts, argc, argv);

    if (opts.test & TEST_TCP_CONNECT)
    {
        for (uint32_t i = 0; opts.count == 0 || i < opts.count; i++)
        {
            if (network_connect(opts.host, opts.port, opts.timeout))
            {
                if (opts.beep & BEEP_MODE_PASS)
                    utils_beep(PASS_FREQ, BEEP_DURATION);

                console_set_color(FOREGROUND_GREEN);
                console_printf("Port '%u' of host '%s' is REACHABLE\n", opts.port, opts.host);
                console_reset();
            }
            else
            {
                if (opts.beep & BEEP_MODE_FAIL)
                    utils_beep(FAIL_FREQ, BEEP_DURATION);

                console_set_color(FOREGROUND_RED);
                console_printf("Port '%u' of host '%s' is UNREACHABLE\n", opts.port, opts.host);
                console_reset();
            }
            if (opts.delay)
                utils_sleep(opts.delay);
        }
    }
    else
    {
        console_printf("error: test is not implemented\n");
        return EXIT_FAILURE;
    }

    network_finalize();
    console_finalize();

    return 0;
}
