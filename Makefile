cflags = \
	$(CPPFLAGS) $(CFLAGS) \
	-std=c11 -flto -s -O3 \
	-Wall -Wextra -Werror \
	-Wno-unused-parameter -Wno-unused-variable

ldflags = \
	$(LDFLAGS) \
	-lWs2_32 -lkernel32

WINDRES ?= windres

sources := console.c main.c network.c options.c utils.c
objects := $(sources:.c=.o) resource.o
depends := $(sources:.c=.d)

all: poke.exe

clean:
	$(RM) *.d *.o *.exe

poke.exe: $(objects)
	$(CC) $(strip $(cflags) -o $@ $(objects) $(ldflags))

.c.o:
	$(CC) $(strip $(cflags) -c -o $@ $<)

resource.o: resource.rc
	$(WINDRES) $(strip -i resource.rc -o $@ -O coff)

-include $(depends)

.PHONY: all clean
